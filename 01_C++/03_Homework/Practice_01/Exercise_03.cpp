/**
 * @file Exercise03.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, 
 			necesitamos un algoritmo que lea el n�mero de desaprobados, aprobados, 
			notables y sobresalientes de una asignatura, y nos devuelva:
			a. El tanto por ciento de alumnos que han superado la asignatura.
			b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la asignatura.
 * @version 4.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int terminalInputDisapproved = 0; 		// Entrada de cantidad de desaprobados a trav�s de terminal
int terminalInputApproved = 0; 			// Entrada de cantidad de aprobados a trav�s de terminal
int terminalInputRemarkable = 0;		// Entrada de cantidad de notables a trav�s de terminal
int terminalInputOutstanding = 0;	 	// Entrada de cantidad de sobresalientes a trav�s de terminal

float percentDisapproved = 0.0;			// Porcentaje desaprobados calculado
float percentApproved = 0.0;			// Porcentaje aprobados calculado
float percentRemarkable = 0.0;			// Porcentaje notables calculado
float percentOutstanding = 0.0;			// Porcentaje sobresalientes caculado
float totalNotes = 0.0;					// Total de notas calulado 

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float PercentofDisapproved(int disapproved,float total);
float PercentofApproved(int approved, float total);
float PercentofRemarkable(int remarkable,float total);
float PercentofOutstanding(int outstanding, float total);
float TotalofNotes(int disapproved, int approved, int remarkable, int outstanding);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the number of disapproved: ";
	cin>>terminalInputDisapproved;
	cout<<"\tWrite the number of approved: ";
	cin>>terminalInputApproved;
	cout<<"\tWrite the number of remarkable: ";
	cin>>terminalInputRemarkable;
	cout<<"\tWrite the number of outstanding: ";
	cin>>terminalInputOutstanding;
}
//=====================================================================================================

void Calculate(){
	totalNotes = TotalofNotes(terminalInputDisapproved, terminalInputApproved,terminalInputRemarkable,terminalInputOutstanding);
	percentDisapproved = PercentofDisapproved(terminalInputDisapproved,totalNotes);
	percentApproved = PercentofApproved(terminalInputApproved,totalNotes);
	percentRemarkable = PercentofRemarkable(terminalInputRemarkable,totalNotes);
	percentOutstanding = PercentofOutstanding(terminalInputOutstanding,totalNotes);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTotal of notes: "<<totalNotes<<"\r\n";
	cout.precision(4);
	cout<<"\tPercent of disapproved: "<<percentDisapproved<<"%"<<"\r\n";
	cout<<"\tPercent of approved: "<<percentApproved<<"%"<<"\r\n";
	cout<<"\tPercent of remarkable: "<<percentRemarkable<<"%"<<"\r\n";
	cout<<"\tPercent of outstanding: "<<percentOutstanding<<"%" <<"\r\n";
}
//=====================================================================================================

float TotalofNotes(int disapproved, int approved, int remarkable, int outstanding){
	return disapproved + approved + remarkable + outstanding;	// total = aprobados + desaprobados + notables + sobresalientes
}
//=====================================================================================================

float PercentofDisapproved(int disapproved,float total){
	return 	(disapproved/total) * 100 ;							// % desaprobados = desaprobados / total
}

//=====================================================================================================
float PercentofApproved(int approved, float total){
	return 	(approved/total) * 100 ;							// % aprobados = aprobados / total
}

//=====================================================================================================
float PercentofRemarkable(int remarkable,float total){
	return 	(remarkable/total) * 100 ;							// % notables = notables / total
}

//=====================================================================================================
float PercentofOutstanding(int outstanding, float total){
	return 	(outstanding/total) * 100 ;							// % sobresalientes = sobresalientes / total
}

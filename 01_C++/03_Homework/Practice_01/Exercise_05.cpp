/**
 * @file Exercise05.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	El costo de un autom�vil nuevo para un comprador es la suma total 
 			del costo del veh�culo, del porcentaje de la ganancia del vendedor
			y de los impuestos locales o estatales aplicables (sobre el precio de venta). 
			Suponer una ganancia del vendedor del 12% en todas las unidades y un impuesto 
			del 6% y dise�ar un algoritmo para leer el costo total del autom�vil e imprimir
			el costo para el consumidor.
 * @version 4.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalImputCarPrice = 0.0; 			// Entrada del costo de venta del veh�culo a trav�s de terminal

float sellerProfit = 0.0; 					// Ganancia del vendedor calculada
float carTaxes = 0.0; 						// Impuesto vehicular calculado
float totalPriceCustomer = 0.0; 			// Costo total de veh�culo calculado
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float PriceCustomer(float carPrice,float profits,float taxes);
float Profits(float carPrice);
float Taxes(float priceCustomer);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the car price: ";
	cin>>terminalImputCarPrice;
}
//=====================================================================================================

void Calculate(){
	sellerProfit = Profits(terminalImputCarPrice);
	carTaxes = Taxes(totalPriceCustomer);
	totalPriceCustomer = PriceCustomer (terminalImputCarPrice,sellerProfit,carTaxes);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTotal to pay for the customer: "<<totalPriceCustomer<<"\r\n";
}
//=====================================================================================================

float Profits(float carPrice){
	return  0.12 * carPrice;				// ganacia = 0.12 * costo_veh�culo
}
//=====================================================================================================

float Taxes(float priceCustomer){
	return 	0.06 * priceCustomer;			// impuesto = 0.06 * costo_total
}
//=====================================================================================================

float PriceCustomer(float carPrice,float profits,float taxes){
	return 	carPrice + profits - taxes;		// costo_total = costo_veh�culo + ganacia - impuesto
}

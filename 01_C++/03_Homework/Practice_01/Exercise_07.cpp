/**
 * @file Exercise07.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, 
 			mediante la siguiente f�rmula:	F = (G*masa1*masa2) / (distancia^2)
			Donde G es la constante de gravitaci�n universal: G = 6.673 * 10-8 (cm^3)/(g.seg^2)
 * @version 2.0
 * @date 31.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define G 6.673 * pow(10,-8.0)			// Constante de gravitaci�n universal

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalInputMass1 = 0.0; 		// Entrada de masa 1 a traves del terminal
float terminalInputMass2 = 0.0;	 		// Entrada de masa 2 a traves del terminal
float terminalInputDistance = 0.0;		// Entrada de la distancia a traves del terminal

float forceOfAttraction = 0.0;			// Fuerza de atracci�n calculada

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateForce(float mass1, float mass2, float distance);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the mass 1: ";
	cin>>terminalInputMass1;
	cout<<"\tWrite the mass 2: ";
	cin>>terminalInputMass2;
	cout<<"\tWrite the distance between the masses: ";
	cin>>terminalInputDistance;
}
//=====================================================================================================

void Calculate(){
	forceOfAttraction = CalculateForce(terminalInputMass1,terminalInputMass2,terminalInputDistance);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe force calculated is: "<<forceOfAttraction<<"\r\n";
}
//=====================================================================================================

float CalculateForce(float mass1, float mass2, float distance){
	return G * mass1 * mass2 / pow(distance,2);			// fuerza = G * masa1 * masa2 / distancia^2
}

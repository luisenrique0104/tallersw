/**
 * @file Exercise02.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Un maestro desea saber que porcentaje de hombres
 			y que porcentaje de mujeres hay en un grupo de estudiantes. 
 * @version 4.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int terminalInputNumbWomen = 0; 		// Entrada de la cantidad de mujeres a trav�s del terminal
int terminalInputNumbMen = 0;	 		// Entrada de la cantidad de hombres a trav�s del terminal

float percentWomen = 0.0;				// Porcentaje calculado de mujeres en el grupo
float percentMen = 0.0;					// Porcentaje calculado de hombres en el grupo
float totalPeople = 0.0;				// Cantidad calculada de personas en el grupo 

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float PercentofWomen(int women,float total);
float PercentofMen(int men, float total);
float TotalofPeople(int women, int men);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the number of women: ";
	cin>>terminalInputNumbWomen;
	cout<<"\tWrite the number of men: ";
	cin>>terminalInputNumbMen;
}
//=====================================================================================================

void Calculate(){
	totalPeople = TotalofPeople(terminalInputNumbWomen, terminalInputNumbMen);
	percentWomen = PercentofWomen(terminalInputNumbWomen,totalPeople);
	percentMen = PercentofMen(terminalInputNumbMen,totalPeople);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTotal of people: "<<totalPeople<<"\r\n";
	cout.precision(4);
	cout<<"\tPercent of women: "<<percentWomen<<"%"<<"\r\n";
	cout<<"\tPercent of men: "<<percentMen<<"%" <<"\r\n";
}
//=====================================================================================================

float TotalofPeople(int women, int men){
	return women + men;				// total = mujeres + hombres
}
//=====================================================================================================

float PercentofWomen(int women,float total){
	return 	(women/total) * 100 ;	// % mujeres = mujeres / total
}

//=====================================================================================================
float PercentofMen(int men, float total){
	return 	(men/total) * 100 ;		// % hombres = hombres / total
}

/**
 * @file Exercise04.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Un departamento de climatolog�a ha realizado recientemente su conversi�n
			 al sistema m�trico. Dise�ar un algoritmo para realizar las siguientes conversiones:
			a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente 
				Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
			b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en 
				mil�metros (25.5 mm = 1pulgada.
 * @version 4.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalImnputCel = 0.0; 		// Entrada de temperatura en Celsius a trav�s del terminal
float terminalInputWater = 0.0; 		// Entrada de cantidad de agua en pulgadas c�bicas

float tempFar = 0.0; 					// Temperatura en Fahrenheit calculada
float mmWater = 0.0; 					// Cantidad de agua en mil�metos c�bicos calculada
	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float TempFahrenheit(float tempCelsius);
float VolumeofWater(float volumeWater);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the temperature in Celsius: ";
	cin>>terminalImnputCel;
	cout<<"\tWrite the volume of water in cubic inches: ";
	cin>>terminalInputWater;
}
//=====================================================================================================

void Calculate(){
	tempFar = TempFahrenheit(terminalImnputCel);
	mmWater = VolumeofWater(terminalInputWater);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTemperture in Fahrenheit: "<<tempFar<<"\r\n";
	cout<<"\tVolumen of wather in cubic millimeters: "<<mmWater<<"\r\n";
}
//=====================================================================================================

float TempFahrenheit(float tempCelsius){
	return 1.8 * tempCelsius + 32;			// temfahrenheit = 9/5 * tempcelsius + 32
}
//=====================================================================================================

float VolumeofWater(float volumeWater){
	return 	volumeWater * pow(25.5,3);		// volumen_agua(mm^3) = volumen_agua * 25.5^3
}

/**
 * @file Exercise06.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Desglosar cierta cantidad de segundos 
 			a su equivalente en d�as, horas, minutos y segundos.
 * @version 2.0
 * @date 28.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int terminalInputSeconds = 0.0; 					// Entrada de segundos a trav�s de terminal

int secondsCalculated = 0; 							// Segundos calculados
int minutesCalculated = 0; 							// Minutos calculados
int minutesConverted = 0;							// Conversi�n de minutos
int hoursCalculated = 0; 							// Horas calculadas
int hoursConverted = 0;								// Conversi�n de horas
int daysCalculated = 0;								// D�as calculados
int daysConverted = 0;								// Conversi�n de d�as

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int SecondsCalculation(int minutesconv);
int MinutesCalculation(int hoursconv);
int MinutesConvertion(int hoursconv);
int HoursCalculation(int daysconv);
int HoursConvertion(int daysconv);
int DaysCalculation(int seconds);
int DaysConvertion(int seconds);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the number of total seconds: ";
	cin>>terminalInputSeconds;
}
//=====================================================================================================

void Calculate(){
	daysCalculated = DaysCalculation(terminalInputSeconds);
	daysConverted = DaysConvertion(terminalInputSeconds);
	hoursCalculated = HoursCalculation(daysConverted);
	hoursConverted = HoursConvertion(daysConverted);
	minutesCalculated = MinutesCalculation(hoursConverted);
	minutesConverted = MinutesConvertion(hoursConverted);
	secondsCalculated = SecondsCalculation(minutesConverted);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTotal days calculated: "<<daysCalculated<<"\r\n";
	cout<<"\tTotal hours calculated: "<<hoursCalculated<<"\r\n";
	cout<<"\tTotal minutes calculated: "<<minutesCalculated<<"\r\n";
	cout<<"\tTotal seconds calculated: "<<secondsCalculated<<"\r\n";
}
//=====================================================================================================

int DaysCalculation(int seconds){
	return	seconds / 86400;						// dias = segundos_totales / 86400
}
//=====================================================================================================

int DaysConvertion(int seconds){
	return	seconds % 86400;						// dias_a_convetir = segundos_totales % 86400
}
//=====================================================================================================

int HoursCalculation(int daysconv){
	return	daysconv / 3600	;						// horas = dias_a_convetir / 3600
}
//================	=====================================================================================

int HoursConvertion(int daysconv){
	return	daysconv % 3600;						// horas_a_convetir = dias_a_convetir % 3600
}
//=====================================================================================================

int MinutesCalculation(int hoursconv){
	return  hoursconv / 60;							// minutos = horas_a_convetir / 60
}
//=====================================================================================================

int MinutesConvertion(int hoursconv){
	return  hoursconv % 60;							// minutos_a_convertir = horas_a_convetir % 60
}
//=====================================================================================================

int SecondsCalculation(int minutesconv){
	return  minutesconv % 60;						// segundos = minutos_a_convertir % 60
}

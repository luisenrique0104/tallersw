/**
 * @file Exercise08.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Calcular el monto final, dados como datos el Capital Inicial, 
 			el  tipo de Inter�s, el numero de periodos por a�o, y el numero de a�os de  la inversi�n. 
			El c�lculo del capital final se basa en la formula del inter�s compuesto.
				M = C(1+i/N)^(N*T)
			Donde:	M = Capital final o Monto,	C = Capital Inicial,	i = Tipo de inter�s nominal
					N = Numero de periodos por a�o,		T = Numero de a�os
			- Si un cliente deposita al Banco la cantidad de $10,000 a inter�s compuesto con una tasa del 8% anual.  
			�Cual ser� el monto que recaude despu�s de 9 a�os? 
 			- �Cuanto debe cobrar el cliente dentro de 3 a�os si deposita  $ 100,000. al 9% anual  y capitaliz�ndose los intereses bimestralmente?   
 * @version 2.0
 * @date 05.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
 

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalInputPrincipal = 0.0; 			// Entrada de la capital a traves del terminal
float terminalInputTime = 0.0;	 				// Entrada del tiempo a traves del terminal
float terminalInputInterest = 0.0;				// Entrada de la tasa de inter�s a traves del terminal
float terminalInputInterestPeriods = 0.0;		// Entrada de periodos de inter�s a traves del terminal

float totalAmount = 0.0;						// Monto total calculado

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float FinalAmount(float principal, float time, float interest, float period);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the Principal: ";
	cin>>terminalInputPrincipal;
	cout<<"\tWrite the time: ";
	cin>>terminalInputTime;
	cout<<"\tWrite the interest: ";
	cin>>terminalInputInterest;
	cout<<"\tWrite the interest periods: ";
	cin>>terminalInputInterestPeriods;
	
	
}
//=====================================================================================================

void Calculate(){
	totalAmount = FinalAmount(terminalInputPrincipal,terminalInputTime,terminalInputInterest,terminalInputInterestPeriods);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe final amount is: "<<totalAmount<<"\r\n";
}
//=====================================================================================================

float FinalAmount(float principal, float time, float interest, float period){
	return principal * pow((1+interest/period),(period*time));			// Monto = capital (1 + interes/periodo) ^ (periodo * tiempo)
}

/**
 * @file Exercise04.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief	Dado un tiempo en minutos, calcular los d�as, horas y minutos que le corresponden.
 * @version 2.0
 * @date 26.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalDaysMax = 1440;				// Cantidad m�xima de minutos por d�a
int globalHoursMax = 60;				// Cantidad m�xima de minutos por hora
int globalTerminalMinutes = 0;			// Entrada de minutos a trav�s del terminal

int daysTotal = 0;						// Cantidad de d�as totales
int hoursTotal = 0;						// Cantidad de horas totales
int minutesTotal = 0;					// Cantidad de minutos totales

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void Calculate();
void ShowResults();
int CalculateDays(int minutes);
int CalculateHours(int minutes);
int CalculateMinutes(int minutes);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the total of minutes: ";
	globalTerminalMinutes = CheckData();
	if(globalTerminalMinutes == -1){
		return Error;
	}
	
	return Success;

}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;	
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
				return -1;
			}
    	}
	}
}

//=====================================================================================================
void Calculate(){
	daysTotal = CalculateDays(globalTerminalMinutes);
	hoursTotal = CalculateHours(globalTerminalMinutes);
	minutesTotal = CalculateMinutes(globalTerminalMinutes);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe given minutes are equal to: "<<daysTotal<<" days, "<<hoursTotal<<" hours and "<<minutesTotal<<" minutes.";
}

//=====================================================================================================
int CalculateDays(int minutes){
	
	int days = 0;
	
	if (minutes > globalDaysMax){
		days = minutes / globalDaysMax;
	}
	
	return days;
}

//=====================================================================================================
int CalculateHours(int minutes){
	
	int hours = 0;
	
	if (minutes > globalHoursMax){
		minutes = minutes % globalDaysMax;
		hours = minutes / globalHoursMax;
	}
	
	return hours;
}

//=====================================================================================================
int CalculateMinutes(int minutes){
	
	minutes = (minutes % globalDaysMax) % globalHoursMax;

		return minutes;
}

//=====================================================================================================

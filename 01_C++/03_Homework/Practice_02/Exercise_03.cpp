/**
 * @file Exercise03.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief 	Ordene de mayor a menor 3 n�meros  ingresados por teclado
 * @version 2.0
 * @date 11.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalTerminalNumber1 = 0.0;				// Entrada del primero n�mero a trav�s del terminal
float globalTerminalNumber2 = 0.0;				// Entrada del segundo n�mero a trav�s del terminal
float globalTerminalNumber3 = 0.0;				// Entrada del tercer n�mero a trav�s del terminal

float biggerNumber = 0.0;						// Numero mayor

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void Calculate();
void ShowResults();
float MaxNumber(float number1, float number2, float number3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\n\tWrite the first number: ";
	globalTerminalNumber1 = CheckData();
	if(globalTerminalNumber1 == -1){
		return Error;
	}
	cout<<"\n\tWrite the second number: ";
	globalTerminalNumber2 = CheckData();
	if(globalTerminalNumber2 == -1){
		return Error;
	}
	cout<<"\n\tWrite the third number: ";
	globalTerminalNumber3 = CheckData();
	if(globalTerminalNumber2 == -1){
		return Error;
	}
	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;		
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
				return -1;
			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	biggerNumber = MaxNumber(globalTerminalNumber1,globalTerminalNumber2,globalTerminalNumber3);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe bigger number is: "<<biggerNumber<<"\r\n";
}

//=====================================================================================================
float MaxNumber(float number1, float number2, float number3){
	int aux = 0;
	
	if (number1 > number2){
		aux = number1;
		if (aux > number3){
			return aux;
		} else {
			return number3;
		}
	} else {
		aux = number2;
		if (aux > number3){
			return aux;
		} else {
			return number3;
		}
	}
}

//=====================================================================================================

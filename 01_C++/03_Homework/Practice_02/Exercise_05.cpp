/**
 * @file Exercise05.cpp
 * @author Luis Cerna (luisenrique0104@gmail.com)
 * @brief	Dados tres datos enteros positivos, que representen las longitudes de un posible triangulo,
 			determine si los datos corresponden a un triangulo. En caso afirmativo, escriba si el 
			triangulo es equil�tero, is�sceles o escaleno. Calcule adem�s su �rea.
 * @version 2.0
 * @date 26.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalTerminalNumber1 = 0;				// Entrada del primero n�mero a trav�s del terminal
int globalTerminalNumber2 = 0;				// Entrada del segundo n�mero a trav�s del terminal
int globalTerminalNumber3 = 0;				// Entrada del tercer n�mero a trav�s del terminal

bool posibleTriangule = false;				// �Es un posible tri�ngulo?

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void Calculate();
void ShowResults();
bool triangulePosibility(int number1, int number2, int number3);
void TrianguleClass(bool triangule, int number1, int number2, int number3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the first number: ";
	globalTerminalNumber1 = CheckData();
	if(globalTerminalNumber1 == -1){
		return Error;
	}
	cout<<"\n\tWrite the second number: ";
	globalTerminalNumber2 = CheckData();
	if(globalTerminalNumber2 == -1){
		return Error;
	}
	cout<<"\n\tWrite the third number: ";
	globalTerminalNumber3 = CheckData();
	if(globalTerminalNumber3 == -1){
		return Error;
	}
	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
				return -1;
			}
		}
    }
}

//=====================================================================================================
void Calculate(){
	posibleTriangule = triangulePosibility(globalTerminalNumber1,globalTerminalNumber2,globalTerminalNumber3);

}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	TrianguleClass(posibleTriangule,globalTerminalNumber1,globalTerminalNumber2,globalTerminalNumber3);
}

//=====================================================================================================
bool triangulePosibility(int number1, int number2, int number3){
	if ((abs(number1 - number2) < number3) && ( number3 < number2 + number1)){
		return true;
	} else {
		return false;
	}
}

//=====================================================================================================
void TrianguleClass(bool triangule, int number1, int number2, int number3){
	if (posibleTriangule == true){
		cout<<"Si es posible formar un triangulo con los numeros dados.\r\n";
		if( (number1 == number2) && (number2 == number3)){
			cout<<"El triangulo seria equilatero.\r\n";
		} else {
			if ((number1 == number2) || (number2 == number3) || (number2 == number3)){
				cout<<"El triangulo seria isosceles.\r\n";
			} else{
				cout<<"El triangulo seria escaleno.\r\n";
			}
		}
	} else {
		cout<<"No es posible formar un triangulo con las longitudes dadas.\r\n";
	}
}

//=====================================================================================================
